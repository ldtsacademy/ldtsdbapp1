package es.ldts.dbapp1;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.content.res.AssetManager;

class filemanager {

    public static String preaddresslocal;
    public static String docroot;
    public static String cacheaddresslocal;
    private static Context context;


    public static void setbook(String cachedir, Context contexto) {
        preaddresslocal = "file:///android_asset/";
        cacheaddresslocal =  cachedir;
        context = contexto;
        docroot = "emdocs/";
        copyFolder("data");
        //test();

    }

    private static void copyFolder(String folder)
    {
        File myDir = new File(cacheaddresslocal, folder);
        if(!myDir.exists()){
            myDir.mkdir();
        }
        AssetManager assetManager = context.getAssets();
        String[] files = null;
        try {
            files = assetManager.list(docroot + folder);
        } catch (IOException e) {
            System.out.println("------ Can not read file: " + e.toString());
        }

        for(String filename : files) {

            boolean isDirectory = false;
            try {
                isDirectory = (assetManager.list(docroot + folder + "/" + filename).length > 0);
            } catch (IOException e) { }

            if (isDirectory) {
                copyFolder(folder + "/" + filename);
            }
            else {
                copyFile(folder,filename);
            }
        }

    }

    private static void copyFile(String folder, String filename) {
        InputStream in = null;
        OutputStream out = null;
        AssetManager assetManager = context.getAssets();

        try {
            in = assetManager.open(docroot + folder + "/" + filename);
            String outDir = cacheaddresslocal + "/" + folder;
            File outFile = new File(outDir, filename);
            out = new FileOutputStream(outFile);
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (IOException e) {
            System.out.println("Failed to copy asset file: " + filename + " - " + e.getMessage());
        }

    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }


}
